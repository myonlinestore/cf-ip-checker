#!/bin/bash

## Run `cp var/ipv4.compare var/ipv4.cache` to mark any IPv4 changes as "processed"
## Run `cp var/ipv6.compare var/ipv6.cache` to mark any IPv6 changes as "processed"

## Script var's
SELF=$(basename "$0")
WORKING_FOLDER=$(dirname "$0")
CONFIG_FILE=$WORKING_FOLDER"/"${SELF/.sh/.cfg}

## Config defaults
CF_IPV4_REMOTE_FILE="https://www.cloudflare.com/ips-v4"
CF_IPV6_REMOTE_FILE="https://www.cloudflare.com/ips-v6"
CACHE_FOLDER=$WORKING_FOLDER"/var/"
IPV4_COMPARE_FILE=$CACHE_FOLDER"ipv4.compare"
IPV6_COMPARE_FILE=$CACHE_FOLDER"ipv6.compare"
IPV4_CACHE_FILE=$CACHE_FOLDER"ipv4.cache"
IPV6_CACHE_FILE=$CACHE_FOLDER"ipv6.cache"
LOGFILE=$CACHE_FOLDER${SELF/.sh/.log}
DATE=`date "+%Y-%m-%d %H:%M:%S"`

## Load config overrides
[ -r "$CONFIG_FILE" ] && . "$CONFIG_FILE"

##
## Main script
##

## Slack alert function
function alert {
    PAYLOAD='payload={"attachments": [{"fallback": "CF IP range checker", "title": "CloudFlare IP ranges", "title_link": "https://www.cloudflare.com/ips/", "pretext": "'$1'", "color": "warning", "text" : "'$2'"}] }'
    curl -s -o /dev/null -X POST --data-urlencode "$PAYLOAD" "$SLACK_URL"
}

## First, fetch the compare files
wget -q $CF_IPV4_REMOTE_FILE -O- --no-check-certificate | sort > $IPV4_COMPARE_FILE
wget -q $CF_IPV6_REMOTE_FILE -O- --no-check-certificate | sort > $IPV6_COMPARE_FILE 

## Exit if this was the first compare for both IPv4 and IPv6
CACHED=0
if [ ! -f $IPV4_CACHE_FILE ]
then
    cp $IPV4_COMPARE_FILE $IPV4_CACHE_FILE
    ((CACHED+=1))
fi

if [ ! -f $IPV6_CACHE_FILE ]
then
    cp $IPV6_COMPARE_FILE $IPV6_CACHE_FILE
    ((CACHED+=1))
fi

## Exit if this was the first compare
if [ $CACHED == 2 ]
then
    MESSAGE="No cache available, therefore skipped notification check"
    echo $MESSAGE
    echo $DATE" : "$MESSAGE >> $LOGFILE
    exit
fi

## Check for IPV4 changes
IPV4_DIFF=`diff -y -W 40 -t $IPV4_CACHE_FILE $IPV4_COMPARE_FILE`
EXIT_CODE=$?
## When diff fails to run, it will have exit code 2
if [ $EXIT_CODE -ne 2 ] 
then
    ## Diff will have exit code "1" when there were differences.
    if [ $EXIT_CODE -eq 1 ]
    then
        CACHE_SIZE=$(wc -c <"$IPV4_CACHE_FILE")
        COMPARE_SIZE=$(wc -c <"$IPV4_COMPARE_FILE")
        if [ $COMPARE_SIZE != $CACHE_SIZE ]
        then
            if [ $COMPARE_SIZE -ge $CACHE_SIZE ]
            then
                PRETEXT="*CloadFlare IPv4 range(s) added:*"
            else
                PRETEXT="*CloadFlare IPv4 range(s) removed:*"
            fi
        else
            PRETEXT="*CloadFlare IPv4 range(s) changed:*"
        fi
        alert "$PRETEXT" "\`\`\`$IPV4_DIFF\`\`\`"
        echo $DATE" : "$PRETEXT" "$IPV4_DIFF >> $LOGFILE
    else
        echo $DATE" : No IPv4 changes" >> $LOGFILE
    fi
else
    PRETEXT="Creating IPv4 diff failed..."
    alert "$PRETEXT"
    echo $DATE" : "$PRETEXT >> $LOGFILE
fi

## Check for IPV6 changes
IPV6_DIFF=`diff -y -W 40 -t $IPV6_CACHE_FILE $IPV6_COMPARE_FILE`
EXIT_CODE=$?
## When diff fails to run, it will have exit code 2
if [ $EXIT_CODE -ne 2 ] 
then
    ## Diff will have exit code "1" when there were differences.
    if [ $EXIT_CODE -eq 1 ]
    then
        CACHE_SIZE=$(wc -c <"$IPV6_CACHE_FILE")
        COMPARE_SIZE=$(wc -c <"$IPV6_COMPARE_FILE")
        if [ $COMPARE_SIZE != $CACHE_SIZE ]
        then
            if [ $COMPARE_SIZE -ge $CACHE_SIZE ]
            then
                PRETEXT="*CloadFlare IPv6 range(s) added:*"
            else
                PRETEXT="*CloadFlare IPv6 range(s) removed:*"
            fi
        else
            PRETEXT="*CloadFlare IPv6 range(s) changed:*"
        fi
        alert "$PRETEXT" "\`\`\`$IPV6_DIFF\`\`\`"
        echo $DATE" : "$PRETEXT" "$IPV6_DIFF >> $LOGFILE
    else
        echo $DATE" : No IPv6 changes" >> $LOGFILE
    fi
else
    PRETEXT="Creating IPv6 diff failed..."
    alert "$PRETEXT"
    echo $DATE" : "$PRETEXT >> $LOGFILE
fi
